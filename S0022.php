<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>売上検索結果表示｜物品売上管理システム</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<style>
	body{
		margin-top: 50px; float: right;
	}
	</style>
	
</head>
<body>
<div class="container">
	<nav class="navbar navbar-static-top navbar-fixed-top navbar-">
		<div class="navbar-header">
		<a class="navbar-brand" href="#">物品売上管理システム</a>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#gnav">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="gnav">
			<ul class="nav navbar-nav">
				<li><a href="dqn.img">ダッシュボード</a></li>
				<li><a href="dqn.img">売上管理</a></li>
				<li><a href="dqn.img">売上検索</a></li>
				<li><a href="dqn.img">アカウント登録</a></li>
				<li><a href="dqn.img">アカウント検索</a></li>
			</ul>
		<div class="collapse navbar-collapse navbar-right" id="nav-collapse">
			<ul class="nav navbar-nav">
				<li><a href="#"><i class="fas fa-sign-out-alt"></i>ログアウト</a></li>
			</ul>

		</div><!-- /.navbar-collapse -->

</nav>
	
	<div class="container">
		<div class="page-header">
			<h1> 売上詳細表示</h1>
		</div>
	</div>
<div class="row" style="margin-top:10px;">
		<form class="form-horizontal">
			<div class="form-group">
					<label class="col-sm-2 control-label">販売日</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">2015/01/15</p>
				    </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">担当</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">イチロー</p>
				    </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">商品カテゴリー</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">食料品</p>
				    </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">商品名</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">からあげ弁当</p>
				    </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">単価</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">450</p>
				    </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">個数</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">3</p>
				    </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">備考</label>
				    <div class="col-sm-10">
				      <p class="form-control-static">今日からの新商品</p>
				    </div>
			</div>
		</form>
</div>
					<form name="form" action="" method="get">
				<div class="row" style="margin-top:10px;">
					<div class="col-md-12 text-center">
					<button type="submit" class="btn btn-primary">
					<i class="fas fa-check"></i> 編　集</button>
					
					<button type="submit" class="btn btn-danger">
					<i class="fas fa-times"></i> 削　除</button>
					
					<button type="submit" class="btn btn-default">
					キャンセル</button>
					</div>
				</div>
			</form>
			
</div><!-- /.container-fluid-->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
