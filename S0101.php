<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>
    <body>
    <title>s0101_ログイン画面</title>
    <style>
    
			
			body{background-color:#eeeeee; margin-top:50px;}
			
     </style>
	<div class="container">
		<div class="page-header text-center">
			<h1>物品売上管理システム</h1>
		       アカウントがロックされています。
		</div>
		<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<form name="form" action="form.cgi" method="get" >
				<input type="text" class="form-control"  placeholder="メールアドレス"><br>
			    <input type="text" class="form-control"  placeholder="パスワード"> <br>  
				<button type="submit" class="btn btn-primary btn-block" >ログイン</button>
			</form>
			</div>
		</div>
     </div><!-- /container --> 
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
