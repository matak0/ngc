<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>アカウント検索結果表示</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	
	<style>
		body{ margin-top:50px;
		}
	</style>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-static-top navbar-fixed-top navbar-default">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">物品売上管理システム</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#gnav">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="gnav">
				<ul class="nav navbar-nav">
					<li><a href="dqn.img">ダッシュボード</a></li>
					<li><a href="dqn.img">売上管理</a></li>
					<li><a href="dqn.img">売上検索</a></li>
					<li><a href="dqn.img">アカウント登録</a></li>
					<li><a href="dqn.img">アカウント検索</a></li>
				</ul>
				<div class="collapse navbar-collapse navbar-right" id="nav-collapse">
					<ul class="nav navbar-nav">
						<li><a href="#">ログアウト</a></li>
					</ul>
				</div>
			</div><!-- /.navbar-collapse -->
		</nav>
		
		<div class="page-heaer">
			<h1>アカウント検索結果表示</h1>
			<hr>
		</div>
			
		<div class="container-fluid">
			
			<table class="table table-default">
				<thead>
					<tr>
						<th style="width:200px">操作</th>
						<th>No</th>
						<th style="width:200px">氏名</th>
						<th style="width:300px">メールアドレス</th>
						<th style="width:200px">権限</th>
						<th style="width:50px">売上</th>
					<tr>
				</thead>
				<tbody>
					<tr>
						<td style="width:100px">
							<a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 編集
							<a href="#" class="btn btn-danger"><i class="fas fa-times"></i> 削除
						</td>
						<td>1</td>
						<td>イチロー</td>
						<td>ichiro@sak.com</td>
						<td>アカウント登録</td>
						<td><td style="width:100px">
							<a href="#" class="btn btn-default">一覧
						</td>
					</tr>
					<tr>
						<td style="width:100px">
							<a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 編集
							<a href="#" class="btn btn-danger"><i class="fas fa-times"></i> 削除
						</td>
						<td>2</td>
						<td>ダルビッシュ　有</td>
						<td>yu.darvish@sak.com</td>
						<td>売上登録</td>
						<td><td style="width:100px">
							<a href="#" class="btn btn-default">一覧
						</td>
					</tr>
					<tr>
						<td style="width:100px">
							<a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 編集
							<a href="#" class="btn btn-danger"><i class="fas fa-times"></i> 削除
						</td>
						<td>3</td>
						<td>田中　将大</td>
						<td>masahiro.tanaka@sak.com</td>
						<td>売上登録</td>
						<td><td style="width:100px">
							<a href="#" class="btn btn-default">一覧
						</td>
					</tr>
					<tr>
						<td style="width:100px">
							<a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 編集
							<a href="#" class="btn btn-danger"><i class="fas fa-times"></i> 削除
						</td>
						<td>4</td>
						<td>松坂　大輔</td>
						<td>daisuke.matsuzaka@sak.com</td>
						<td>権限なし</td>
						<td><td style="width:100px">
							<a href="#" class="btn btn-default">一覧
						</td>
					</tr>
					<tr>
						<td style="width:100px">
							<a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 編集
							<a href="#" class="btn btn-danger"><i class="fas fa-times"></i> 削除
						</td>
						<td>5</td>
						<td>本田　圭介</td>
						<td>keisuke.honda@sak.com</td>
						<td>アカウント登録</td>
						<td><td style="width:100px">
							<a href="#" class="btn btn-default">一覧
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div>全件数：#</div>
		
	<nav aria-label="Page navigation">
		<ul class="pagination">
			<li>
				<a href="#" aria-label="Previous">back
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#">6</a></li>
				<li><a href="#">7</a></li>
				<li><a href="#">8</a></li>
				<li><a href="#">9</a></li>
				<li><a href="#">10</a></li>
			<li>
				<a href="#" aria-label="Next">next
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
		</ul>
</nav>
		
		<div class="btn-toolbar" role="toolbar" aria-label="...">
  <div class="btn-group" role="group" aria-label="...">...</div>
  <div class="btn-group" role="group" aria-label="...">...</div>
  <div class="btn-group" role="group" aria-label="...">...</div>
</div>
		
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>