<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>売上検索結果表示｜物品売上管理システム</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<style>
	body{
		margin-top: 50px;
	}
	.nav{ font-color: #DDD; }
	</style>
	
</head>
<body>
<div class="container">
	<nav class="navbar navbar-static-top navbar-fixed-top navbar-default">
		<div class="navbar-header">
		<a class="navbar-brand" href="#">物品売上管理システム</a>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#gnav">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="gnav">
			<ul class="nav navbar-nav">
				<li><a href="dqn.img">ダッシュボード</a></li>
				<li><a href="dqn.img">売上管理</a></li>
				<li><a href="dqn.img">売上検索</a></li>
				<li><a href="dqn.img">アカウント登録</a></li>
				<li><a href="dqn.img">アカウント検索</a></li>
			</ul>
		<div class="collapse navbar-collapse navbar-right" id="nav-collapse">
			<ul class="nav navbar-nav">
				<li><a href="#"><i class="fas fa-sign-out-alt"></i>ログアウト</a></li>
			</ul>

		</div><!-- /.navbar-collapse -->

</nav>
	
	<div class="container">
		<div class="page-header">
			<h1>売上検索結果表示</h1>
		</div>
	</div>

			
			<table class="table table-striped">
			<thead>
				<tr>
					<th style="width:100px;">操作</th>
					<th>No</th>
					<th>販売日</th>
					<th>担当</th>
					<th>商品カテゴリー</th>
					<th>商品名</th>
					<th>単価</th>
					<th>個数</th>
					<th>小計</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>1</td>
					<td>2015/1/15</td>
					<td>イチロー</td>
					<td>食料品</td>
					<td>から揚げ弁当</td>
					<td>450</td>
					<td>3</td>
					<td>1,350</td>
					
				</tr>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>2</td>
					<td>2015/1/15</td>
					<td>イチロー</td>
					<td>食料品</td>
					<td>あんぱん</td>
					<td>120</td>
					<td>10</td>
					<td>1,200</td>
				</tr>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>3</td>
					<td>2015/1/15</td>
					<td>イチロー</td>
					<td>飲料</td>
					<td>コカコーラ 500ml</td>
					<td>130</td>
					<td>5</td>
					<td>650</td>
				</tr>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>4</td>
					<td>2015/1/15</td>
					<td>本田 圭祐</td>
					<td>その他</td>
					<td>Yシャツ白 M</td>
					<td>1,200</td>
					<td>2</td>
					<td>2,400</td>
				</tr>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>5</td>
					<td>2015/1/14</td>
					<td>錦織圭</td>
					<td>食料品</td>
					<td>とろっと玉子のオムライスドリア</td>
					<td>380</td>
					<td>8</td>
					<td>3,040</td>
				</tr>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>6</td>
					<td>2015/1/14</td>
					<td>錦織 圭</td>
					<td>本・雑誌</td>
					<td>週刊少年マガジン2015年7月号</td>
					<td>250</td>
					<td>15</td>
					<td>3,750</td>
				</tr>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>7</td>
					<td>2015/1/14</td>
					<td>池田 勇太</td>
					<td>本・雑誌</td>
					<td>NHKラジオ ラジオ英会話2015年02月号</td>
					<td>780</td>
					<td>1</td>
					<td>780</td>
				</tr>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>8</td>
					<td>2015/1/14</td>
					<td>本田 圭祐</td>
					<td>食料品</td>
					<td>チーズケーキ</td>
					<td>220</td>
					<td>4</td>
					<td>880</td>
				</tr>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>9</td>
					<td>2015/1/14</td>
					<td>本田 圭祐</td>
					<td>食料品</td>
					<td>麻婆＆エビチリ炒飯</td>
					<td>520</td>
					<td>9</td>
					<td>4,680</td>
				</tr>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>10</td>
					<td>2015/1/14</td>
					<td>本田 圭祐</td>
					<td>飲料</td>
					<td>アクエリアス 2L</td>
					<td>260</td>
					<td>2</td>
					<td>520</td>
				</tr>
				<tr>
					<td><a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細</a></td>
					<td>11</td>
					<td>2015/1/14</td>
					<td>錦織 圭</td>
					<td>その他</td>
					<td>靴下 黒 28cm</td>
					<td>800</td>
					<td>1</td>
					<td>800</td>
				</tr>
			</tbody>
		</table>
		
</div><!-- /.container-fluid-->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
