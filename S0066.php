
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>商品詳細編集</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<style>
		body{margin-top:50px; padding:10px;}
		
	</style>
	
</head>
<body

<div class="container">
		<div class="page-header">
 	 	<div class="row">
 	 	<div class="text-center">
 	 		<h1> ログアウト確認</h1>
 	 		<form class="form-horizontal">
 	 		<div class="form-group">
 	 		<div class="col-md-3">
 	 		</div>
				<div class="col-md-5">
					<input type="name" class="form-control" id="author" placeholder="ログアウトします。よろしいですか？">
				</div>
			</div>
		
 	 		

<div class="text-center">
					<button type="submit" class="btn btn-primary"><i class="fas fa-check"></i> はい</button>
					<button type="submit" class="btn btn-danger"> いいえ</button>
				</div>
			</form>
 		</div>
 		</div>
 		</div>
 </div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>
