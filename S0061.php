<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>商品検索結果表示</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	
	<style>
		body{ margin-top:50px;
		}
	</style>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-static-top navbar-fixed-top navbar-default">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">物品売上管理システム</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#gnav">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="gnav">
				<ul class="nav navbar-nav">
					<li><a href="dqn.img">ダッシュボード</a></li>
					<li><a href="dqn.img">売上管理</a></li>
					<li><a href="dqn.img">売上検索</a></li>
					<li><a href="dqn.img">アカウント登録</a></li>
					<li><a href="dqn.img">アカウント検索</a></li>
				</ul>
				<div class="collapse navbar-collapse navbar-right" id="nav-collapse">
					<ul class="nav navbar-nav">
						<li><a href="#">ログアウト</a></li>
					</ul>
				</div>
			</div><!-- /.navbar-collapse -->
		</nav>
		
		<div class="page-heaer">
			<h1>商品検索結果表示</h1>
			<br>
		</div>
			<div>売上合計個数：#</div>
			<div>売上合計金額：#</div>
			<hr>
		<div class="container-fluid">
			
			<table class="table table-default">
				<thead>
					<tr>
						<th style="width:100px">操作</th>
						<th style="width:100px">No</th>
						<th style="width:200px">商品カテゴリー</th>
						<th style="width:300px">商品名</th>
						<th style="width:150px">単価</th>
						<th style="width:200px">売上個数</th>
					<tr>
				</thead>
				<tbody>
					<tr>
						<td style="width:100px">
							<a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細
						</td>
						<td>1</td>
						<td>食料品</td>
						<td>からあげ弁当</td>
						<td>450</td>
						<td>3</td>
					</tr>
					<tr>
						<td style="width:100px">
							<a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細
						</td>
						<td>2</td>
						<td>食料品</td>
						<td>あんぱん</td>
						<td>120</td>
						<td>3</td>
					</tr>
					<tr>
						<td style="width:100px">
							<a href="#" class="btn btn-primary"><i class="fas fa-check"></i> 詳細
						</td>
						<td>3</td>
						<td></td>
						<td>コカコーラ 500ml</td>
						<td>130</td>
						<td>10</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div>全件数：#</div>
		
	<nav aria-label="Page navigation">
		<ul class="pagination">
			<li>
				<a href="#" aria-label="Previous">back
					<span aria-hidden="true">&laquo;</span>
				</a>
				<a href="#" aria-label="Next">next
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
		</ul>
</nav>
		
		<div class="btn-toolbar" role="toolbar" aria-label="...">
  <div class="btn-group" role="group" aria-label="...">...</div>
  <div class="btn-group" role="group" aria-label="...">...</div>
  <div class="btn-group" role="group" aria-label="...">...</div>
</div>
		
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>