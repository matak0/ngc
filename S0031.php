<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>アカウント登録</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<style>
		body{margin-top:50px; padding:10px;}
		
	</style>
	
</head>
<body>

	<nav class="navbar navbar-static-top navbar-fixed-top navbar-default">
		<div class="navbar-header">
		<a class="navbar-brand" href="#">物品売上管理システム</a>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#gnav">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="gnav">
			<ul class="nav navbar-nav">
				<li><a href="dqn.img">ダッシュボード</a></li>
				<li><a href="dqn.img">売上管理</a></li>
				<li><a href="dqn.img">売上検索</a></li>
				<li><a href="dqn.img">アカウント登録</a></li>
				<li><a href="dqn.img">アカウント検索</a></li>
			</ul>
		<div class="collapse navbar-collapse navbar-right" id="nav-collapse">
			<ul class="nav navbar-nav">
				<li><a href="#"><i class="fas fa-sign-out-alt"></i>ログアウト</a></li>
			</ul>
		</div>
	</div><!-- /.navbar-collapse -->

</nav>

	<div class="container">
		<div class="page-header">
 	 	<div class="row">
 	 		<h1> アカウントを登録してよろしいですか？</h1>
 	 		<form class="form-horizontal">
				<div class="form-group">
				<label for="name" class="col-md-5 control-label"> 氏名<span class="badge badge-default">必須</span></label>
				<div class="col-md-5">
					<input type="name" class="form-control" id="author" placeholder="イチロー">
				</div></div>
 	 			<div class="form-group">
				<label for="email" class="col-md-5 control-label"> メールアドレス<span class="badge badge-default">必須</span></label>
				<div class="col-md-5">
					<input type="email" class="form-control" id="author" placeholder="ichiro@sak.com">
				</div></div>
 	 			<div class="form-group">
				<label for="inputPassword3" class="col-md-5 control-label"> パスワード<span class="badge badge-default">必須</span></label>
				<div class="col-md-5">
					<input type="password" class="form-control" id="author" placeholder="・・・・">
				</div></div>
				<div class="form-group">
				<label for="inputPassword3" class="col-md-5 control-label"> パスワード確認<span class="badge badge-default">必須</span></label>
				<div class="col-md-5">
					<input type="password" class="form-control" id="inputEmail3" placeholder="・・・・">
				</div></div>
				<div class="form-group">
						<label for="radio" class="col-md-5 control-label"> 権限<span class="badge badge-default">必須</span></label>
					<div class="col-md-5">
						<div class="radio"> 
							<label><input type="radio" value="1" name="mong"> 権限なし</label>
							<label><input type="radio" value="1" name="mong"> 参照</label>
	        				<label><input type="radio" value="1" name="mong" checked> 更新</label>
 	 					</div>
 	 				</div>
 	 			</div>
 	 		</div>
 	 		</div>
 	 		</form>
			<div class="text-center">
			<button type="submit" class="btn btn-primary"> ＯＫ</button>
			<button type="submit" class="btn btn-default"> キャンセル</button>
 	 		</div>
 		</div>
 	</div>
 	

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>
