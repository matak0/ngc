<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>ダッシュボード</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    
   <style>
       body{margin:20px;}
       
       [class *= col-]
        {border:1px solid #ccc;}
        
     
    </style>
   
</head>
<body>
		<nav class="navbar navbar-static-top navbar-fixed-top navbar-default">
		<div class="navbar-header">
		<a class="navbar-brand" href="#">物品売上管理システム</a>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#gnav">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="gnav">
			<ul class="nav navbar-nav">
				<li class="active"><a href="dqn.img">ダッシュボード</a></li>
				<li><a href="dqn.img">売上管理</a></li>
				<li><a href="dqn.img">売上検索</a></li>
				<li><a href="dqn.img">アカウント登録</a></li>
				<li><a href="dqn.img">アカウント検索</a></li>
			</ul>
		<div class="collapse navbar-collapse navbar-right" id="nav-collapse">
			<ul class="nav navbar-nav">
				<li><a href="#"><i class="fas fa-sign-out-alt"></i>ログアウト</a></li>
			</ul>

		</div>
		</div><!-- /.navbar-collapse -->

       <div class="text text-right">前回ログイン時間 2016/06/23 11:59</div>
       <div class="container">
       <h1>イチローさんようこそ</h1>
       
    		<table class="table table-striped">
			<thead>
				<tr>
					
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>今月の売上個数：123個</td>
					<td>全体の売上個数：1235個</td>
					
				</tr>
				<tr>
				　	<td>今月の売上金額：12345円個</td>
					<td>全体の売上個金額：123456円</td>
				
				</tr>	
			</tbody>
		</table>
  
		</div><!-- /container --> 

	
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
